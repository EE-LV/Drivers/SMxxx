﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_2009_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 2009\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_2009_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 2009\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the instrument driver for the SMxxx &lt;SM5/SM300/SM400&gt;. The VI Tree displays all the user-callable VIs of the instrument driver in an organized table.

Instrument manufacturer:    SM-Elektronik
Instrument model numbers: SM5, SM300, SM400
Intrument type:                    Motor Control

Dependencies:
- ASCII.lvlib

Copyright (C) 2009  Holger Brand, H.Brand@gsi.de

GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstrasse 1
64291 Darmstadt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SMxxx BCC.vi" Type="VI" URL="../SMxxu/SMxxx BCC.vi"/>
		<Item Name="SMxxx Command Transaction.vi" Type="VI" URL="../SMxxu/SMxxx Command Transaction.vi"/>
		<Item Name="SMxxx Format Command.vi" Type="VI" URL="../SMxxu/SMxxx Format Command.vi"/>
		<Item Name="SMxxx Read Answer.vi" Type="VI" URL="../SMxxu/SMxxx Read Answer.vi"/>
		<Item Name="SMxxx Send Command.vi" Type="VI" URL="../SMxxu/SMxxx Send Command.vi"/>
		<Item Name="SMxxx Utility Clean Up Initialize.vi" Type="VI" URL="../SMxxu/SMxxx Utility Clean Up Initialize.vi"/>
		<Item Name="SMxxx Utility Default Instrument Setup.vi" Type="VI" URL="../SMxxu/SMxxx Utility Default Instrument Setup.vi"/>
		<Item Name="SMxxx VISA Write-Read.vi" Type="VI" URL="../SMxxu/SMxxx VISA Write-Read.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Getting Started" Type="Folder">
			<Item Name="Application Example" Type="Folder">
				<Item Name="SMxxx Test Command Sub.vi" Type="VI" URL="../SMxxx/SMxxx Test Command Sub.vi"/>
				<Item Name="SMxxx Read Complete Status Sub.vi" Type="VI" URL="../SMxxu/SMxxx Read Complete Status Sub.vi"/>
				<Item Name="SMxxx Manual Move Sub.vi" Type="VI" URL="../SMxxu/SMxxx Manual Move Sub.vi"/>
			</Item>
			<Item Name="SMxxx Enable Menu.vi" Type="VI" URL="../SMxxx/SMxxx Enable Menu.vi"/>
			<Item Name="SMxxx Test Command.vi" Type="VI" URL="../SMxxx/SMxxx Test Command.vi"/>
			<Item Name="SMxxx Read Complete Status.vi" Type="VI" URL="../SMxxx/SMxxx Read Complete Status.vi"/>
			<Item Name="SMxxx Manual Move.vi" Type="VI" URL="../SMxxx/SMxxx Manual Move.vi"/>
		</Item>
		<Item Name="CNC" Type="Folder">
			<Item Name="SMxxx CNC Fill Ringbuffer.vi" Type="VI" URL="../SMxxx/SMxxx CNC Fill Ringbuffer.vi"/>
			<Item Name="SMxxx CNC End.vi" Type="VI" URL="../SMxxx/SMxxx CNC End.vi"/>
			<Item Name="SMxxx CNC Initialize.vi" Type="VI" URL="../SMxxx/SMxxx CNC Initialize.vi"/>
			<Item Name="SMxxx CNC Interpreter Start.vi" Type="VI" URL="../SMxxx/SMxxx CNC Interpreter Start.vi"/>
			<Item Name="SMxxx CNC Interpreter Continue.vi" Type="VI" URL="../SMxxx/SMxxx CNC Interpreter Continue.vi"/>
			<Item Name="SMxxx CNC Interpreter Stop.vi" Type="VI" URL="../SMxxx/SMxxx CNC Interpreter Stop.vi"/>
			<Item Name="SMxxx CNC Status.vi" Type="VI" URL="../SMxxx/SMxxx CNC Status.vi"/>
		</Item>
		<Item Name="Configuration" Type="Folder">
			<Item Name="SMxxx Controller Release.vi" Type="VI" URL="../SMxxx/SMxxx Controller Release.vi"/>
			<Item Name="SMxxx Echo Off.vi" Type="VI" URL="../SMxxx/SMxxx Echo Off.vi"/>
			<Item Name="SMxxx Enable Frontpanel.vi" Type="VI" URL="../SMxxx/SMxxx Enable Frontpanel.vi"/>
			<Item Name="SMxxx Feed Rate.vi" Type="VI" URL="../SMxxx/SMxxx Feed Rate.vi"/>
			<Item Name="SMxxx Read Parameter.vi" Type="VI" URL="../SMxxx/SMxxx Read Parameter.vi"/>
			<Item Name="SMxxx Save Parameter To Memory.vi" Type="VI" URL="../SMxxx/SMxxx Save Parameter To Memory.vi"/>
			<Item Name="SMxxx Set Direction.vi" Type="VI" URL="../SMxxx/SMxxx Set Direction.vi"/>
			<Item Name="SMxxx Set Parameter.vi" Type="VI" URL="../SMxxx/SMxxx Set Parameter.vi"/>
			<Item Name="SMxxx Automatic InPosition Message.vi" Type="VI" URL="../SMxxu/SMxxx Automatic InPosition Message.vi"/>
		</Item>
		<Item Name="Action/Status" Type="Folder">
			<Item Name="SMxxx CNC Command.vi" Type="VI" URL="../SMxxx/SMxxx CNC Command.vi"/>
			<Item Name="SMxxx Go Fast.vi" Type="VI" URL="../SMxxx/SMxxx Go Fast.vi"/>
			<Item Name="SMxxx Override Value.vi" Type="VI" URL="../SMxxx/SMxxx Override Value.vi"/>
			<Item Name="SMxxx Positioning Status.vi" Type="VI" URL="../SMxxx/SMxxx Positioning Status.vi"/>
			<Item Name="SMxxx Reference Run.vi" Type="VI" URL="../SMxxx/SMxxx Reference Run.vi"/>
			<Item Name="SMxxx Reference Run Flag.vi" Type="VI" URL="../SMxxx/SMxxx Reference Run Flag.vi"/>
			<Item Name="SMxxx Start Linear Interpolation.vi" Type="VI" URL="../SMxxx/SMxxx Start Linear Interpolation.vi"/>
			<Item Name="SMxxx Status Controller Release.vi" Type="VI" URL="../SMxxx/SMxxx Status Controller Release.vi"/>
			<Item Name="SMxxx Status Register.vi" Type="VI" URL="../SMxxx/SMxxx Status Register.vi"/>
			<Item Name="SMxxx Status SPS.vi" Type="VI" URL="../SMxxx/SMxxx Status SPS.vi"/>
			<Item Name="SMxxx Stop Positioning.vi" Type="VI" URL="../SMxxx/SMxxx Stop Positioning.vi"/>
			<Item Name="SMxxx Switch Output.vi" Type="VI" URL="../SMxxx/SMxxx Switch Output.vi"/>
			<Item Name="SMxxx Switch SPS.vi" Type="VI" URL="../SMxxx/SMxxx Switch SPS.vi"/>
			<Item Name="SMxxx Write Set Position.vi" Type="VI" URL="../SMxxx/SMxxx Write Set Position.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="SMxxx Input Switch.vi" Type="VI" URL="../SMxxx/SMxxx Input Switch.vi"/>
			<Item Name="SMxxx Read Direction.vi" Type="VI" URL="../SMxxx/SMxxx Read Direction.vi"/>
			<Item Name="SMxxx Read Position.vi" Type="VI" URL="../SMxxx/SMxxx Read Position.vi"/>
			<Item Name="SMxxx Read Set Position.vi" Type="VI" URL="../SMxxx/SMxxx Read Set Position.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="SMxxx Error Query.vi" Type="VI" URL="../SMxxx/SMxxx Error Query.vi"/>
			<Item Name="SMxxx Error Message.vi" Type="VI" URL="../SMxxx/SMxxx Error Message.vi"/>
			<Item Name="SMxxx Reset.vi" Type="VI" URL="../SMxxx/SMxxx Reset.vi"/>
			<Item Name="SMxxx Revision Query.vi" Type="VI" URL="../SMxxx/SMxxx Revision Query.vi"/>
			<Item Name="SMxxx Self-Test.vi" Type="VI" URL="../SMxxx/SMxxx Self-Test.vi"/>
		</Item>
		<Item Name="SMxxx Close.vi" Type="VI" URL="../SMxxx/SMxxx Close.vi"/>
		<Item Name="SMxxx Initialize.vi" Type="VI" URL="../SMxxx/SMxxx Initialize.vi"/>
	</Item>
	<Item Name="Typedefs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SMxxx VISA Cluster.ctl" Type="VI" URL="../SMxxu/SMxxx VISA Cluster.ctl"/>
	</Item>
	<Item Name="SMxxx VI Tree.vi" Type="VI" URL="../SMxxx/SMxxx VI Tree.vi"/>
</Library>
